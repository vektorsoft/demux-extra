/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.jfx.gui.file;

/**
 * Contains constants used in this bundle.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public final class JFXFileMenuConstants {
    
    public static final String FILE_MENU_ID = "com.vektorsoft.demux.jfx.gui.file.fileMenu";
    
    public static final String NEW_FILE_MENU_ITEM = "com.vektorsoft.demux.jfx.gui.file.newFileItem";
    
    public static final String OPEN_FILE_MENU_ITEM = "com.vektorsoft.demux.jfx.gui.file.openFileItem";
    
    public static final String CLOSE_FILE_MENU_ITEM = "com.vektorsoft.demux.jfx.gui.file.closeFileItem";
    
    public static final String SAVE_FILE_MENU_ITEM = "com.vektorsoft.demux.jfx.gui.file.saveFileItem";
    
    public static final String SAVE_AS_FILE_MENU_ITEM = "com.vektorsoft.demux.jfx.gui.file.saveAsFileItem";
    
    public static final String NEW_FILE_TOOLBAR_BUTTON = "com.vektorsoft.demux.jfx.gui.file.newFileButton";
    
    public static final String OPEN_FILE_TOOLBAR_BUTTON = "com.vektorsoft.demux.jfx.gui.file.openFileButton";
    
    public static final String SAVE_FILE_TOOLBAR_BUTTON = "com.vektorsoft.demux.jfx.gui.file.saveFileButton";
    
    public static final String SAVE_AS_FILE_TOOLBAR_BUTTON = "com.vektorsoft.demux.jfx.gui.file.saveAsFileButton";

    private JFXFileMenuConstants(){
        // prevent instantiation
    }
}
