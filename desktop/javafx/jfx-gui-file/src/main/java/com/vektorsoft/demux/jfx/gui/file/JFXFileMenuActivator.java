
package com.vektorsoft.demux.jfx.gui.file;

import com.vektorsoft.demux.core.app.DMXAbstractActivator;
import com.vektorsoft.demux.core.app.DMXAdapterTrackerHandler;
import com.vektorsoft.demux.desktop.gui.DMXMenuService;
import com.vektorsoft.demux.desktop.gui.DMXToolbarService;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * An implementation of {@code com.vektorsoft.demux.core.app.DMXAbstractActivator} class. This class contains logic needed
 * for supporting extension callbacks. Use this class as a starting point for you bundle activators.
 *
 * This class uses {@code SampleTrackerHandler} to track registration of {@code DMXAdapter} service.
 * 
 */
public class JFXFileMenuActivator extends DMXAbstractActivator {
    
    private ServiceTracker menuServiceTracker;
    private ServiceTracker toolbarServiceTracker;

    @Override
    protected void onBundleStart(BundleContext ctx) {
        menuServiceTracker = new ServiceTracker(ctx, DMXMenuService.class, new MenuServiceCustomizer(ctx));
        menuServiceTracker.open();
        
        toolbarServiceTracker = new ServiceTracker(ctx, DMXToolbarService.class, new ToolbarServiceCustomizer(ctx));
        toolbarServiceTracker.open();
    }

    @Override
    protected void onBundleStop(BundleContext ctx) {
       menuServiceTracker.close();
       toolbarServiceTracker.close();
    }

    @Override
    public DMXAdapterTrackerHandler getHandler() {
        return new AdapterTrackerHandler();
    }

}
