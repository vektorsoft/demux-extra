/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.jfx.gui.file;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.resources.ResourceHandlerType;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.desktop.gui.DMXMenuService;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class MenuServiceCustomizer implements ServiceTrackerCustomizer<DMXMenuService, DMXMenuService<Menu, MenuItem>>{
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(MenuServiceCustomizer.class);
    
    private final BundleContext ctx;
    
    public MenuServiceCustomizer(BundleContext bc){
        this.ctx = bc;
    }

    @Override
    public DMXMenuService<Menu, MenuItem> addingService(ServiceReference<DMXMenuService> sr) {
        DMXMenuService<Menu, MenuItem> svc = ctx.getService(sr);
        LOGGER.debug("Menu service found");
        
        DefaultResourceHandler handler = svc.getResourceManager().getResourceHandler(ResourceHandlerType.RESOURCE_BUNDLE);
        
        JFXFileMenu menu = new JFXFileMenu(handler);
        
        svc.insertAtIndex(0, menu);
        
        svc.registerControllerForAction(JFXFileMenuConstants.NEW_FILE_MENU_ITEM, AppFileConstants.CTRL_NEW_FILE);
        svc.registerControllerForAction(JFXFileMenuConstants.OPEN_FILE_MENU_ITEM, AppFileConstants.CTRL_OPEN_FILE);
        svc.registerControllerForAction(JFXFileMenuConstants.CLOSE_FILE_MENU_ITEM, AppFileConstants.CTRL_CLOSE_CURRENT_FILE);
        svc.registerControllerForAction(JFXFileMenuConstants.SAVE_FILE_MENU_ITEM, AppFileConstants.CTRL_SAVE_FILE);
        svc.registerControllerForAction(JFXFileMenuConstants.SAVE_AS_FILE_MENU_ITEM, AppFileConstants.CTRL_SAVE_AS_FILE);
        
//        svc.addResourceListener(menu);
        
        return svc;
    }

    @Override
    public void modifiedService(ServiceReference<DMXMenuService> sr, DMXMenuService<Menu, MenuItem> t) {
        
    }

    @Override
    public void removedService(ServiceReference<DMXMenuService> sr, DMXMenuService<Menu, MenuItem> t) {
        
    }

}
