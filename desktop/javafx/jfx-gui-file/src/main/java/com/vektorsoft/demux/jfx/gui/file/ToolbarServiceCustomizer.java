/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.jfx.gui.file;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.core.resources.ResourceHandlerType;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.desktop.gui.DMXToolbarService;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class ToolbarServiceCustomizer implements ServiceTrackerCustomizer<DMXToolbarService, DMXToolbarService<ToolBar>>{
    
    private final BundleContext ctx;
    
    public ToolbarServiceCustomizer(BundleContext ctx){
        this.ctx = ctx;
    }

    @Override
    public DMXToolbarService<ToolBar> addingService(ServiceReference<DMXToolbarService> sr) {
        DMXToolbarService<ToolBar> svc = ctx.getService(sr);
        DefaultResourceHandler handler = svc.getResourceManager().getResourceHandler(ResourceHandlerType.RESOURCE_BUNDLE);
        
        svc.setToolbarStyle("-fx-background-color: #336699;");
        
        JFXFileToolbar jfxtoolbar = new JFXFileToolbar(handler);
        
        svc.insertToolbarAtIndex(0, jfxtoolbar);
        svc.registerControllerForAction(JFXFileMenuConstants.NEW_FILE_TOOLBAR_BUTTON, AppFileConstants.CTRL_NEW_FILE);
        svc.registerControllerForAction(JFXFileMenuConstants.OPEN_FILE_TOOLBAR_BUTTON, AppFileConstants.CTRL_OPEN_FILE);
        svc.registerControllerForAction(JFXFileMenuConstants.SAVE_FILE_TOOLBAR_BUTTON, AppFileConstants.CTRL_SAVE_FILE);

        
        return svc;
    }

    @Override
    public void modifiedService(ServiceReference<DMXToolbarService> sr, DMXToolbarService<ToolBar> t) {
        
    }

    @Override
    public void removedService(ServiceReference<DMXToolbarService> sr, DMXToolbarService<ToolBar> t) {
        
    }

}
