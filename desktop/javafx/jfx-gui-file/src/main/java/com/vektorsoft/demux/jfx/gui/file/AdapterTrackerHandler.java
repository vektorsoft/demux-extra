
package com.vektorsoft.demux.jfx.gui.file;

import com.vektorsoft.demux.core.app.DMXAdapterTrackerHandler;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.resources.ResourceHandlerType;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;

/**
 * Simple implementation of {@code ServiceTracker} used to track changes to {@code DMXAdapter} service.
 * 
 */
public class AdapterTrackerHandler extends DMXAdapterTrackerHandler {

    @Override
    public void onAdapterAdded(DMXAdapter adapter) {
       // load resource bundles
        DefaultResourceHandler handler = adapter.getResourceManager().getResourceHandler(ResourceHandlerType.RESOURCE_BUNDLE);
        handler.loadResourceBundle("bundles.strings", this.getClass().getClassLoader());
        handler.loadResourceBundle("bundles.AssetsBundle", this.getClass().getClassLoader());
    }

    @Override
    public void onAdapterModified(DMXAdapter adapter) {
        // add here logic needed to run when adapter is modfied
    }

    @Override
    public void onAdapterRemoved(DMXAdapter adapter) {
        // add here logic needed to run when adapter is removed
    }

}
