/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.jfx.gui.file;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.ModelChangeListener;
import com.vektorsoft.demux.core.resources.ResourceEvent;
import com.vektorsoft.demux.core.resources.ResourceListener;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class JFXFileToolbar extends ToolBar implements ResourceListener, ModelChangeListener {
    
    private final DefaultResourceHandler handler;
    
    private final Button newFileButton;
    private final Button openFileButton;
    private final Button saveFileButton;
    private final Button saveasFileButton;
    
    public JFXFileToolbar(DefaultResourceHandler handler){
        this.handler = handler;
        
        newFileButton = new Button();
        newFileButton.setId(JFXFileMenuConstants.NEW_FILE_TOOLBAR_BUTTON);
        
         openFileButton = new Button();
        openFileButton.setId(JFXFileMenuConstants.OPEN_FILE_TOOLBAR_BUTTON);
        
        saveFileButton = new Button();
        saveFileButton.setDisable(true);
        saveFileButton.setId(JFXFileMenuConstants.SAVE_FILE_TOOLBAR_BUTTON);
        
        saveasFileButton = new Button();
        saveasFileButton.setDisable(true);
        saveasFileButton.setId(JFXFileMenuConstants.SAVE_AS_FILE_TOOLBAR_BUTTON);
       
        getItems().addAll(newFileButton, openFileButton, new Separator(Orientation.VERTICAL), saveFileButton, saveasFileButton);
        
    }
    

    @Override
    public void onResourceEvent(ResourceEvent event) {
        newFileButton.setGraphic(new ImageView((Image)handler.getResource("jfx.gui.file.new.menuitem.icon")));
        newFileButton.setTooltip(new Tooltip(handler.getString("jfx.gui.toolbar.newfile.tooltip")));

        openFileButton.setGraphic(new ImageView((Image) handler.getResource("jfx.gui.file.open.menuitem.icon")));
        openFileButton.setTooltip(new Tooltip(handler.getString("jfx.gui.toolbar.openfile.tooltip")));

        saveFileButton.setGraphic(new ImageView((Image) handler.getResource("jfx.gui.file.save.menuitem.icon")));
        saveFileButton.setTooltip(new Tooltip(handler.getString("jfx.gui.toolbar.savefile.tooltip")));

        saveasFileButton.setGraphic(new ImageView((Image) handler.getResource("jfx.gui.file.saveas.menuitem.icon")));
        saveasFileButton.setTooltip(new Tooltip(handler.getString("jfx.gui.toolbar.saveasfile.tooltip")));
    }

    @Override
    public void modelDataChanged(DMXModelEvent event) {
        if(event.getType() == DMXModelEvent.ModelEventType.DATA_UPDATED){
            DMXFile curFile = (DMXFile)event.getNewValues().get(AppFileConstants.VAR_CURRENT_FILE);
            if(curFile == null){
                saveFileButton.setDisable(true);
                saveasFileButton.setDisable(true);
            } else {
                saveFileButton.setDisable(false);
                saveasFileButton.setDisable(false);
            }
        }
    }
    

}
