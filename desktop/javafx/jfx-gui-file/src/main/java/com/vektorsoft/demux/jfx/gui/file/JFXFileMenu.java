/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.jfx.gui.file;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.ModelChangeListener;
import com.vektorsoft.demux.core.resources.ResourceEvent;
import com.vektorsoft.demux.core.resources.ResourceListener;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class JFXFileMenu extends Menu implements ResourceListener, ModelChangeListener {
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(JFXFileMenu.class);
    
    private final DefaultResourceHandler handler;
    
    private final  MenuItem newFileItem;
    private final MenuItem openFileItem;
    private final MenuItem closeFileItem;
    private final MenuItem saveMenuItem;
    private final MenuItem saveAsMenuItem;
    
    public JFXFileMenu(DefaultResourceHandler handler){
        this.handler = handler;
        
        
        setId(JFXFileMenuConstants.FILE_MENU_ID);
        setMnemonicParsing(true);
        
        newFileItem = new MenuItem();
        newFileItem.setId(JFXFileMenuConstants.NEW_FILE_MENU_ITEM);
        getItems().add(newFileItem);
        
        openFileItem = new MenuItem();
        openFileItem.setId(JFXFileMenuConstants.OPEN_FILE_MENU_ITEM);
        getItems().add(openFileItem);
       
        closeFileItem = new MenuItem();
        closeFileItem.setId(JFXFileMenuConstants.CLOSE_FILE_MENU_ITEM);
        closeFileItem.setDisable(true);
        getItems().add(closeFileItem);
        
        getItems().add(new SeparatorMenuItem());
       
        saveMenuItem = new MenuItem();
        saveMenuItem.setId(JFXFileMenuConstants.SAVE_FILE_MENU_ITEM);
        saveMenuItem.setDisable(true);
        getItems().add(saveMenuItem);
        
        saveAsMenuItem = new MenuItem();
        saveAsMenuItem.setId(JFXFileMenuConstants.SAVE_AS_FILE_MENU_ITEM);
        saveAsMenuItem.setDisable(true);
        getItems().add(saveAsMenuItem);
        
       
    }
    
    
    @Override
    public void onResourceEvent(ResourceEvent event) {
        setText(handler.getString("jfx.gui.file.menu"));
        newFileItem.setText(handler.getString("jfx.gui.file.new.menuitem"));
        newFileItem.setGraphic(new ImageView((Image)handler.getResource("jfx.gui.file.new.menuitem.icon")));
        newFileItem.setAccelerator((KeyCombination)handler.getResource("jfx.gui.file.new.menuitem.accel"));
        
        openFileItem.setText(handler.getString("jfx.gui.file.open.menuitem"));
         openFileItem.setAccelerator((KeyCombination)handler.getResource("jfx.gui.file.open.menuitem.accel"));
        openFileItem.setGraphic(new ImageView((Image)handler.getResource("jfx.gui.file.open.menuitem.icon")));
        
        closeFileItem.setText(handler.getString("jfx.gui.file.close.menuitem"));
        closeFileItem.setAccelerator((KeyCombination)handler.getResource("jfx.gui.file.close.menuitem.accel"));
        closeFileItem.setGraphic(new ImageView((Image)handler.getResource("jfx.gui.file.close.menuitem.icon")));
        
        saveMenuItem.setText(handler.getString("jfx.gui.file.save.menuitem"));
        saveMenuItem.setAccelerator((KeyCombination)handler.getResource("jfx.gui.file.save.menuitem.accel"));
        saveMenuItem.setGraphic(new ImageView((Image)handler.getResource("jfx.gui.file.save.menuitem.icon")));
        
        saveAsMenuItem.setText(handler.getString("jfx.gui.file.saveas.menuitem"));
        saveAsMenuItem.setGraphic(new ImageView((Image)handler.getResource("jfx.gui.file.saveas.menuitem.icon")));
    }

    @Override
    public void modelDataChanged(DMXModelEvent event) {
        if(event.getType() == DMXModelEvent.ModelEventType.DATA_UPDATED){
            LOGGER.debug("Changed data: " + event.getNewValues());
            DMXFile curFile = (DMXFile)event.getNewValues().get(AppFileConstants.VAR_CURRENT_FILE);
            if(curFile == null){
                closeFileItem.setDisable(true);
                saveMenuItem.setDisable(true);
                saveAsMenuItem.setDisable(true);
            } else {
                closeFileItem.setDisable(false);
                saveMenuItem.setDisable(false);
                saveAsMenuItem.setDisable(false);
            }
        }
    }

}
