package bundles;

import java.util.ListResourceBundle;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;

/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

/**
 * Contains binary resources for the bundle.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class AssetsBundle extends ListResourceBundle{

    @Override
    protected Object[][] getContents() {
        return new Object[][]{
            {"jfx.gui.file.new.menuitem.icon", new Image(this.getClass().getClassLoader().getResource("/icons/filenew.png").toString()) },
            {"jfx.gui.file.new.menuitem.accel", KeyCombination.keyCombination("Ctrl+N") },
            {"jfx.gui.file.open.menuitem.icon", new Image(this.getClass().getClassLoader().getResource("/icons/fileopen.png").toString()) },
            {"jfx.gui.file.open.menuitem.accel", KeyCombination.keyCombination("Ctrl+O") },
            {"jfx.gui.file.close.menuitem.icon", new Image(this.getClass().getClassLoader().getResource("/icons/fileclose.png").toString()) },
            {"jfx.gui.file.close.menuitem.accel", KeyCombination.keyCombination("Ctrl+L") },
            {"jfx.gui.file.save.menuitem.icon", new Image(this.getClass().getClassLoader().getResource("/icons/filesave.png").toString()) },
            {"jfx.gui.file.save.menuitem.accel", KeyCombination.keyCombination("Ctrl+S") },
            {"jfx.gui.file.saveas.menuitem.icon", new Image(this.getClass().getClassLoader().getResource("/icons/filesaveas.png").toString()) }
        };
    }

}
