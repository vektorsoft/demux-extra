/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.extra.jfx.fileview;

import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXView;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class CSVFileViewProducer implements FileViewProducer{

    @Override
    public DMXView createFileViewForType(DMXFile file, DMXAdapter adapter) {
        DMXView view = new CSVFileView((String)file.getCurrentContent(), adapter, "com.vektorsoft.demux.jfx.appfiles." + System.nanoTime());
        view.constructUI();
        return view;
    }

    @Override
    public boolean isFileTypeSupported(DMXFile file) {
        boolean supported = false;
        for(String type : file.getMimeTypes()){
            if("text/csv".equals(type)){
                supported = true;
                break;
            }
        }
        return supported;
    }

}
