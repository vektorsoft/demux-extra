/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.extra.jfx.fileview;

import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import javafx.scene.Node;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class CSVFileView extends TextFileView {
    
    private HBox hbox;
    private ToggleButton textButton;
    private ToggleButton gridButton;
    private ToggleGroup group;

    public CSVFileView(String content, DMXAdapter adapter, String viewId, String... dataIds){
        super(content, adapter,viewId,dataIds);
        group = new ToggleGroup();
        
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void constructUI() {
        hbox = new HBox();
        textButton = new ToggleButton("Text");
        textButton.setToggleGroup(group);
        textButton.setSelected(true);
        gridButton = new ToggleButton("Grid");
        gridButton.setToggleGroup(group);
        
        hbox.getChildren().addAll(textButton, gridButton);
        root.setTop(hbox);
    }
    
    

}
