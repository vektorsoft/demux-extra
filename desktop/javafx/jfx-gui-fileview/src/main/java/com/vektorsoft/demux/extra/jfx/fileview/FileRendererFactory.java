/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.extra.jfx.fileview;

import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXView;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple mapping class for 
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class FileRendererFactory {

    private static final List<FileViewProducer> PRODUCERS;
    private static final FileRendererFactory INSTANCE;
    private static DMXAdapter adapter;
    public static boolean initialized = false;
    
    /**
     * Fallback producer for text files.
     */
    private TextFileViewProducer textProducer;
    
    static {
        PRODUCERS = new ArrayList<>();
        INSTANCE = new FileRendererFactory();
    }
    
    public static void init(DMXAdapter ad){
        adapter = ad;
        initialized = true;
    }
    
    public static FileRendererFactory getInstance(){
        if(!initialized){
            throw new IllegalStateException("Not initialized. Call FileRendererFactory.init() first");
        }
        return INSTANCE;
    }
    
    private FileRendererFactory(){
        textProducer = new TextFileViewProducer();
    }
    
    public void registerProducer(FileViewProducer producer){
        PRODUCERS.add(producer);
    }
    
    public void removeProducer(FileViewProducer producer){
        PRODUCERS.remove(producer);
    }
    
    public DMXView getFileRenderer(DMXFile file) {
        for (FileViewProducer producer : PRODUCERS) {
            if (producer.isFileTypeSupported(file)) {
                return producer.createFileViewForType(file, adapter);
            }
        }
        if(textProducer.isFileTypeSupported(file)){
            return textProducer.createFileViewForType(file, adapter);
        }
        throw new IllegalArgumentException("Unsupported file type for file " + file.getName());
    }
}
