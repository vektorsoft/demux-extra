/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.extra.jfx.fileview;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.core.mva.DMXAbstractView;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class TextFileView extends DMXAbstractView implements ChangeListener<String> {

    private TextArea textArea;
    protected BorderPane root;
    protected final DMXAdapter adapter;
    protected final String viewId;
    protected String parentViewId;

    private String content;

    public TextFileView(String content, DMXAdapter adapter, String viewId, String... dataIds) {
        super(adapter, dataIds);
        this.adapter = adapter;
        this.viewId = viewId;
        this.content = content != null ? content : "";

    }

    @Override
    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        adapter.invokeController(AppFileConstants.CTRL_SYNC_CONTROLLER, textArea.getText());
    }


    @Override
    public void render() {
        if(!textArea.getText().equals(content)){
            textArea.setText(content);
        }
    }

    @Override
    public void updateFromModel(DMXLocalModel data) {
        if (ids.length > 0) {
            content = data.get(ids[0], String.class);
        }
    }

    @Override
    public Object getViewUI() {
        return root;
    }

    @Override
    public void constructUI() {
        textArea = new TextArea();
        textArea.setText(content);
        root = new BorderPane(textArea);

        textArea.textProperty().addListener(this);
    }

    @Override
    public String getViewId() {
        return viewId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setParentViewId(String parentViewId) {
        this.parentViewId = parentViewId;
    }

    @Override
    public String getParentViewId() {
        return parentViewId;
    }
    
    

}
