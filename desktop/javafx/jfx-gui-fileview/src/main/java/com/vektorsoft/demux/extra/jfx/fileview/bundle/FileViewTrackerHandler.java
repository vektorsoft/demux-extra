
package com.vektorsoft.demux.extra.jfx.fileview.bundle;

import com.vektorsoft.demux.core.app.DMXAdapterTrackerHandler;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.extra.jfx.fileview.CSVFileViewProducer;
import com.vektorsoft.demux.extra.jfx.fileview.FileRendererFactory;
import com.vektorsoft.demux.extra.jfx.fileview.MultiFileTabView;

/**
 * Simple implementation of {@code ServiceTracker} used to track changes to {@code DMXAdapter} service.
 * 
 */
public class FileViewTrackerHandler extends DMXAdapterTrackerHandler {

    @Override
    public void onAdapterAdded(DMXAdapter adapter) {
        FileRendererFactory.init(adapter);
        FileRendererFactory.getInstance().registerProducer(new CSVFileViewProducer());
        adapter.registerView(MultiFileTabView.class);
    }

    @Override
    public void onAdapterModified(DMXAdapter adapter) {
        // add here logic needed to run when adapter is modfied
    }

    @Override
    public void onAdapterRemoved(DMXAdapter adapter) {
        // add here logic needed to run when adapter is removed
    }

}
