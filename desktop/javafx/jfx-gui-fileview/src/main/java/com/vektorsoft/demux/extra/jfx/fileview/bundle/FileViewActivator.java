
package com.vektorsoft.demux.extra.jfx.fileview.bundle;

import com.vektorsoft.demux.core.app.DMXAbstractActivator;
import com.vektorsoft.demux.core.app.DMXAdapterTrackerHandler;
import org.osgi.framework.BundleContext;

/**
 *
 * An implementation of {@code com.vektorsoft.demux.core.app.DMXAbstractActivator} class. This class contains logic needed
 * for supporting extension callbacks. Use this class as a starting point for you bundle activators.
 *
 * This class uses {@code SampleTrackerHandler} to track registration of {@code DMXAdapter} service.
 * 
 */
public class FileViewActivator extends DMXAbstractActivator {
    

    @Override
    protected void onBundleStart(BundleContext ctx) {
        // add here logic required to run when bundle starts
    }

    @Override
    protected void onBundleStop(BundleContext ctx) {
       // add here logic required to run when bundle stops   
    }

    @Override
    public DMXAdapterTrackerHandler getHandler() {
        return new FileViewTrackerHandler();
    }

}
