/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.extra.jfx.fileview;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.common.appfiles.DMXOPenFilesList;
import com.vektorsoft.demux.core.annotations.DMXModelData;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.desktop.jfx.view.JFXRootView;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
@DMXViewDeclaration (viewID = "com.vektorsoft.demux.extra.jfx.gui.MainFileView", parentViewId = JFXRootView.JFX_ROOT_VIEW_ID)
public class MultiFileTabView {

    @DMXViewDeclaration.ViewUI
    private TabPane mainPane;
    
    @DMXModelData(id = AppFileConstants.VAR_OPEN_FILES_LIST)
    private DMXOPenFilesList openFiles;
    
    @DMXModelData(id = AppFileConstants.VAR_CURRENT_FILE)
    private DMXFile currentFile;
    
    private final DMXAdapter adapter;
    
    private final Map<DMXFile, Tab> fileTabMap;
    
    public MultiFileTabView(DMXAdapter adapter){
        this.adapter = adapter;
        openFiles = new DMXOPenFilesList();
        fileTabMap = new HashMap<>();
    }
    
   
    
    public Tab getTabForFile(DMXFile file){
        for(Tab tab : mainPane.getTabs()){
            DMXFile user = (DMXFile)tab.getUserData();
            if(file.equals(user)){
                return tab;
            }
        }
        return null;
    }
    
    
    @DMXViewDeclaration.Render
    public void render(){
        List<DMXFile> files = openFiles.getFiles();
        if(files.size() > fileTabMap.size()){
            DMXFile file = files.get(files.size() - 1);
            FileTab tab = new FileTab(file, adapter);
            fileTabMap.put(file, tab);
            mainPane.getTabs().add(tab);
        } else if(files.size() < fileTabMap.size()) {
            Iterator<DMXFile> it = fileTabMap.keySet().iterator();
            while(it.hasNext()){
                DMXFile next = it.next();
                if(!files.contains(next)){
                    mainPane.getTabs().remove(fileTabMap.get(next));
                    fileTabMap.remove(next);
                    break;
                }
            }
        } 
        if (currentFile != null) {
            Tab tab = fileTabMap.get(currentFile);
            if (tab != null) {
                mainPane.getSelectionModel().select(fileTabMap.get(currentFile));
                StringBuilder sb = new StringBuilder(currentFile.getName());
                if (currentFile.isDirty()) {
                    sb.append("*");
                }
                fileTabMap.get(currentFile).setText(sb.toString());
            }

        }
        
    }
    
    @DMXViewDeclaration.ConstructUI
    public void makeUI(){
        mainPane = new TabPane();
        mainPane.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            if (oldValue != null && newValue != null && !newValue.equals(oldValue)) {
                for (Map.Entry<DMXFile, Tab> entry : fileTabMap.entrySet()) {
                    if (entry.getValue().equals(newValue)) {
                        adapter.invokeController(AppFileConstants.CTRL_SET_CURRENT_FILE, entry.getKey());
                        break;
                    }
                }
            }
           
        });
       
    }
    

    public DMXOPenFilesList getOpenFiles() {
        return openFiles;
    }

    public void setOpenFiles(DMXOPenFilesList openFiles) {
        this.openFiles = openFiles;
    }

    public DMXFile getCurrentFile() {
        return currentFile;
    }

    public void setCurrentFile(DMXFile currentFile) {
        this.currentFile = currentFile;
    }

    public TabPane getMainPane() {
        return mainPane;
    }
    
}
