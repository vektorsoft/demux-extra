/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.extra.jfx.fileview;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXView;
import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.control.Tab;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class FileTab extends Tab{

    private final DMXFile file;
    private final DMXView fileView;
    
    public FileTab(DMXFile file, DMXAdapter adapter){
        this.file = file;
        StringBuilder sb = new StringBuilder(file.getName());
        if(file.isDirty()){
            sb.append("*");
        }
        setText(sb.toString());
        
        fileView = FileRendererFactory.getInstance().getFileRenderer(file);
        setContent((Node)fileView.getViewUI());
        setClosable(true);
        
        setOnCloseRequest((Event event) -> {
            event.consume();
            adapter.invokeController(AppFileConstants.CTRL_CLOSE_CURRENT_FILE);
        });
    }
    
    
}
