/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.extra.jfx.fileview;

import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import javafx.scene.Node;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public abstract class  FileViewRenderer{
    
    protected DMXFile file;
    protected DMXAdapter adapter;
    
    protected FileViewRenderer(DMXFile file, DMXAdapter adapter){
        this.file = file;
        this.adapter = adapter;
    }

    public DMXFile  getFile(){
        return file;
    }
    
    public abstract Node getView();
    
    public abstract void render();
    
}
