<?xml version="1.0" encoding="UTF-8"?>
<!--
/******************************************************************************

Copyright 2012 - 2014 Vektor Software.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law o<relativePath>../pom.xml</relativePath>r agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*****************************************************************************/
-->
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
    <groupId>org.sonatype.oss</groupId>
    <artifactId>oss-parent</artifactId>
    <version>9</version>
  </parent>
    <groupId>com.vektorsoft.demux.extra</groupId>
    <artifactId>demux-extra</artifactId>
    <version>1.0.0</version>
    <packaging>pom</packaging>
    <name>DEMUX Framework - Extra components</name>
    
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <checkstyle.config>${basedir}/config/checkstyle/checkstyle-vektorsoft.xml</checkstyle.config>
        <license.header>${basedir}/config/license-header.txt</license.header>
        <pmd.target.jdk>1.6</pmd.target.jdk>
        <!-- Used for automatic license header inclusion in NetBeans -->
        <netbeans.hint.license>vektor-apache20</netbeans.hint.license>
    </properties>

    <organization>
        <name>Vektorsoft Ltd.</name>
        <url>http://www.vektorsoft.com</url>
    </organization>
    
    <inceptionYear>2014</inceptionYear>
    
    <licenses>
        <license>
            <name>Apache License 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>
    
    <url>http://demux.vektorsoft.com</url>
    
    <scm>
        <url>https://bitbucket.org/vektorsoft/demux/src</url>
        <connection>scm:git:https://bitbucket.org/vektorsoft/demux-extra.git</connection>
    </scm>
    
    <developers>
        <developer>
            <id>dev0</id>
            <name>Vladimir Djurovic</name>
            <email>vdjurovic@vektorsoft.com</email>
            <organization>Vektorsoft Ltd.</organization>
        </developer>
    </developers>
    
    <profiles>
        <profile>
            <id>release-sign</id>
            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>
            <build>
                 <plugins>
            <!-- sgining configuration -->
                     <plugin>
                         <groupId>org.apache.maven.plugins</groupId>
                         <artifactId>maven-gpg-plugin</artifactId>
                         <version>1.4</version>
                         <executions>
                             <execution>
                                 <id>sign-artifacts</id>
                                 <phase>verify</phase>
                                 <goals>
                                     <goal>sign</goal>
                                 </goals>
                             </execution>
                         </executions>
                     </plugin>
                 </plugins>
            </build>
        </profile>
    </profiles>
    
    
    <dependencies>
        <dependency>
            <groupId>org.apache.felix</groupId>
            <artifactId>org.apache.felix.framework</artifactId>
            <version>4.2.1</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.10</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>1.9.5</version>
            <scope>test</scope>
        </dependency>
    </dependencies>
    
    
    <build>
        <pluginManagement>
            <plugins>
                
                <!-- Configuration of bundle plugin -->
                <plugin>
                    <groupId>org.apache.felix</groupId>
                    <artifactId>maven-bundle-plugin</artifactId>
                    <version>2.5.3</version>
                    <configuration>
                        <instructions>
                            <Bundle-DocURL>http://demux.vektorsoft.com</Bundle-DocURL>
                        </instructions>
                    </configuration>
                </plugin>
                
                <!-- Configuration for Surefire test reporting plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>2.17</version>
                    <configuration>
                        <useSystemClassLoader>true</useSystemClassLoader>
<!--                         Required to make CObertura work with Java 7. 
                        <argLine>-XX:-UseSplitVerifier</argLine>-->
                    </configuration>
                </plugin>
                
                <!-- Configuration for Checkstyle code check plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <version>2.10</version>
                    <configuration>
                        <configLocation>${checkstyle.config}</configLocation>
                        <failsOnError>false</failsOnError>
                        <headerLocation>${license.header}</headerLocation>
                    </configuration>
                </plugin>
                
                <!-- Configuration of PMD plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-pmd-plugin</artifactId>
                    <version>3.3</version>
                    <configuration>
                        <targetJdk>${pmd.target.jdk}</targetJdk>
                        <ruleset>/rulesets/unusedcode</ruleset>
                        <ruleset>/rulesets/basic</ruleset>
                        <ruleset>/rulesets/design</ruleset>
                        <ruleset>/rulesets/braces</ruleset>
                        <ruleset>/rulesets/optimizations</ruleset>
                    </configuration>
                </plugin>
                
                <!-- Configuration for FindBugs plugin -->
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>findbugs-maven-plugin</artifactId>
                    <version>3.0.0</version>
                    <configuration>
                    </configuration>
                </plugin>
                
                <!-- Configuration for Cobertura test coverage plugin -->
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>cobertura-maven-plugin</artifactId>
                    <version>2.5.2</version>
                </plugin>
                
                <!-- Configuration of Maven versions plugin -->
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>versions-maven-plugin</artifactId>
                    <version>2.0</version>
                </plugin>
                
                <!-- Configuration of Javadoc plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>2.9</version>
                    <executions>
                        <execution>
                            <phase>deploy</phase>
                            <goals>
                                <goal>jar</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <additionalparam>-Xdoclint:none</additionalparam>
                    </configuration>
                </plugin>
                
                <!-- Configuration of source plugin -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>2.2.1</version>
                    <executions>
                        <execution>
                            <phase>deploy</phase>
                            <goals>
                                <goal>jar</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>
   
    <reporting>
        <plugins>
            <!-- Configuration for Checkstyle code check plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>2.6</version>
                <configuration>
                    <configLocation>${checkstyle.config}</configLocation>
                    <failsOnError>false</failsOnError>
                    <headerLocation>${license.header}</headerLocation>
                </configuration>
            </plugin>
            
            <!-- Configuration of PMD plugin -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <version>3.3</version>
                <configuration>
                    <targetJdk>${pmd.target.jdk}</targetJdk>
                    <ruleset>/rulesets/unusedcode</ruleset>
                    <ruleset>/rulesets/basic</ruleset>
                    <ruleset>/rulesets/design</ruleset>
                    <ruleset>/rulesets/braces</ruleset>
                    <ruleset>/rulesets/optimizations</ruleset>
                </configuration>
            </plugin>
                
            <!-- Configuration for FindBugs plugin -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>findbugs-maven-plugin</artifactId>
                <version>3.0.0</version>
            </plugin>
                
            <!-- Configuration for Cobertura test coverage plugin -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>cobertura-maven-plugin</artifactId>
                <version>2.5.2</version>
            </plugin>
        </plugins>
    </reporting>

</project>