/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.ctrl;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.common.appfiles.DMXOPenFilesList;
import com.vektorsoft.demux.core.mva.DMXAbstractController;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.ExecutionException;

/**
 * Controller for synchronizing file content with current file view being edited.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SyncController extends DMXAbstractController {
    
    /**
     * Creates new instance.
     */
    public SyncController(){
        super(AppFileConstants.VAR_CURRENT_FILE);
    }

    @Override
    public String getMapping() {
        return AppFileConstants.CTRL_SYNC_CONTROLLER;
    }

    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        if(params.length != 1){
            throw new IllegalArgumentException("Require content as argument");
        }
        DMXFile currentFile = model.get(AppFileConstants.VAR_CURRENT_FILE, DMXFile.class);
//       DMXOPenFilesList list = model.get(AppFileConstants.VAR_OPEN_FILES_LIST, DMXOPenFilesList.class);
//       int fileId = (Integer)params[0];
       Object content = params[0];
//       DMXFile file = list.findFileById(fileId);
       if(currentFile != null){
           currentFile.setCurrentContent(content);
       }
       model.set(AppFileConstants.VAR_CURRENT_FILE, currentFile);
    }

}
