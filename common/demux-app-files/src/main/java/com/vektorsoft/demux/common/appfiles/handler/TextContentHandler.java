/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.handler;

import java.io.UnsupportedEncodingException;

/**
 * <p>
 *  Content handler for text files. This handler will convert file content to/from string.
 * </p>
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class TextContentHandler implements DMXFileContentHandler {    
    
    @Override
    public byte[] toByteArray(Object object) {
        if(!(object instanceof String)){
            throw new IllegalArgumentException("Object must be of type java.lang.String");
        }
        try {
            return ((String)object).getBytes("UTF-8");
        } catch(UnsupportedEncodingException ex){
            throw new RuntimeException(ex);
        }
        
    }

    @Override
    public String fromByteArray(byte[] data) {
        try {
            return new String(data, "UTF-8");
        } catch(UnsupportedEncodingException ex){
            throw new RuntimeException(ex);
        }
        
    }

    @Override
    public boolean isMimeTypeSupported(String mimeType) {
        if(mimeType.startsWith("text/")){
            return true;
        }
        return false;
    }

}
