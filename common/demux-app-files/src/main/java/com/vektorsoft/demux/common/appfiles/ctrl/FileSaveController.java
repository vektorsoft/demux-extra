/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.ctrl;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.common.appfiles.DMXOPenFilesList;
import com.vektorsoft.demux.core.dlg.DialogCustomizer;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.DMXAbstractController;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.ExecutionException;
import java.io.File;

/**
 * <p>
 *  Controller which performs saving of the current file.
 * </p>
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class FileSaveController extends DMXAbstractController {
    
    /**
     * Logger for this class.
     */
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(FileSaveController.class);
    
    /**
     * Current adapter instance.
     */
    private final DMXAdapter adapter;
    
    /**
     * Creates new instance.
     * 
     * @param adapter adapter to set
     */
    public FileSaveController(DMXAdapter adapter){
        super(AppFileConstants.VAR_CURRENT_FILE);
        this.adapter = adapter;
    }

    @Override
    public String getMapping() {
        return AppFileConstants.CTRL_SAVE_FILE;
    }
    
    

    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {       
        
        DMXFile file = model.get(AppFileConstants.VAR_CURRENT_FILE, DMXFile.class);
        if(file.getPath() != null){
            file.save();
        } else {
            File[] target = adapter.getDialogFactory().createFileChooserDialog(DialogCustomizer.FileChooseType.SAVE, null);
            if(target != null && target.length == 1){
                file.setTarget(target[0]);
                file.save();
            }
        }
        
        model.set(AppFileConstants.VAR_CURRENT_FILE, file);
        LOGGER.debug("Saving to file " + file.getPath());
    }

}
