/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.handler.internal;

import com.vektorsoft.demux.common.appfiles.handler.DMXFileContentHandler;
import com.vektorsoft.demux.common.appfiles.handler.SerializableContentHandler;
import com.vektorsoft.demux.common.appfiles.handler.TextContentHandler;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Registry for available file content handlers. Each new content handler must be registered in order to be used for specified MIME
 * type.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public final class FileContentHandlerRegistry {
    
    /**
     * Singleton instance.
     */
    private static final FileContentHandlerRegistry INSTANCE;
    
    /**
     * Static initializer.
     */
    static {
        INSTANCE = new FileContentHandlerRegistry();
    }

    /**
     * List of registered content handler.
     */
    private final List<DMXFileContentHandler> handlers;
    
    /**
     * Content handler for text files.
     */
    private final TextContentHandler textHandler;
    
    /**
     * Creates new singleton instance.
     */
    private FileContentHandlerRegistry(){
        handlers = new ArrayList<DMXFileContentHandler>();
        textHandler = new TextContentHandler();
        handlers.add(new SerializableContentHandler());
    }
    
    /**
     * Static method to get singleton instance.
     * @return instance
     */
    public static FileContentHandlerRegistry getInstance(){
        return INSTANCE;
    }
    
    /**
     * Registers specified content handler. After registration, handler is available for use.
     * 
     * @param handler handler to register
     */
    public void registerContentHandler(DMXFileContentHandler handler){
        handlers.add(handler);
    }
    
    /**
     * Finds content handler for specified MIME type. If type is text type, and no specific 
     * handlers are registered, an instance of {@link TextContentHandler}. is returned. If no handler 
     * matches this type, and {@link  IllegalArgumentException} is thrown.
     * 
     * @param mimeType type
     * @return content handler
     */
    public DMXFileContentHandler getContentHandler(String mimeType){
        if(isTextType(mimeType)){
            return textHandler;
        }
        for(DMXFileContentHandler contentHandler : handlers){
            if(contentHandler.isMimeTypeSupported(mimeType)){
                return contentHandler;
            }
        }
        throw new IllegalArgumentException("Unsupported content type: " + mimeType);
    }
    
    /**
     * Returns content handler for one of the specified MIME types. Each type is checked in sequence, and 
     * first correct handler is received.
     * 
     * @param mimeTypes list of MIME type
     * @return content handler
     */
    public DMXFileContentHandler getContentHandler(Collection<String> mimeTypes){
        DMXFileContentHandler handler = null;
        for (String mimeType : mimeTypes) {
            for (DMXFileContentHandler contentHandler : handlers) {
                if (contentHandler.isMimeTypeSupported(mimeType)) {
                    handler = contentHandler;
                    break;
                }
            }
        }
        // check if this is text file
        if(handler == null){
            for(String type : mimeTypes){
                if(isTextType(type)){
                    handler = textHandler;
                    break;
                }
            }
        }
        if(handler == null){
            throw new IllegalArgumentException("Unsupported content types: " + mimeTypes);
        }
        return handler;
    }
    
    /**
     * Check if specified type is textual type.
     * 
     * @param mimeType MIME type
     * @return <code>true</code> if this is text type, <code>false</code> otherwise
     */
    private boolean isTextType(String mimeType){
            if(mimeType.startsWith("text/")){
                return true;
            }
        return false;
    }
}

