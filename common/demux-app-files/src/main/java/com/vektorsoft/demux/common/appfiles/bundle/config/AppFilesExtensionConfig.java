/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.bundle.config;

import com.vektorsoft.demux.common.appfiles.handler.DMXFileContentHandler;
import com.vektorsoft.demux.core.extension.DefaultExtensionConfig;

/**
 *  Configuration class for extensions to this bundle. In addition to registering controllers and views, 
 * it also provides a way to register new file content handlers. This is performed in extension bundles by 
 * overriding {@link #getAdditionalContentHandlers() } method.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class AppFilesExtensionConfig extends DefaultExtensionConfig {

    
    /**
     * <p>
     * Registers new file content handlers. This method is meant to be overriden by extension bundles. For example:
     * </p>
     * <pre>
     *    public DMXFileContentHandler[] getAdditionalContentHandlers(){
     *       return new DMXFileContentHandler[new MyHandler(), new AnotherHandler()];
     *   }
     * </pre>
     * 
     * @return array of additional content handlers to register
     */
    public DMXFileContentHandler[] getAdditionalContentHandlers(){
        return new DMXFileContentHandler[0];
    }
}
