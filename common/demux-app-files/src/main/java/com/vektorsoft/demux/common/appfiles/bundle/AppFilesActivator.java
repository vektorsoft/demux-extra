/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.bundle;


import com.vektorsoft.demux.common.appfiles.bundle.config.AppFilesExtensionConfig;
import com.vektorsoft.demux.common.appfiles.handler.DMXFileContentHandler;
import com.vektorsoft.demux.common.appfiles.handler.internal.FileContentHandlerRegistry;
import com.vektorsoft.demux.core.app.DMXAbstractActivator;
import com.vektorsoft.demux.core.app.DMXAdapterTrackerHandler;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import org.osgi.framework.BundleContext;

/**
 *
 * An implementation of {@code com.vektorsoft.demux.core.app.DMXAbstractActivator} class. This class contains logic needed
 * for supporting extension callbacks. Use this class as a starting point for you bundle activators.
 *
 * This class uses {@code SampleTrackerHandler} to track registration of {@code DMXAdapter} service.
 * 
 */
public class AppFilesActivator extends DMXAbstractActivator {
    
    /**
     * Logger for this class.
     */
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(AppFilesActivator.class);

    @Override
    protected void onBundleStart(BundleContext ctx) {
        if(extensionConfig instanceof AppFilesExtensionConfig){
            AppFilesExtensionConfig config = (AppFilesExtensionConfig)extensionConfig;
            for(DMXFileContentHandler handler : config.getAdditionalContentHandlers()){
                LOGGER.debug("Registering content handler " + handler.getClass().getName());
                FileContentHandlerRegistry.getInstance().registerContentHandler(handler);
            }
        }
    }

    @Override
    protected void onBundleStop(BundleContext ctx) {
       // add here logic required to run when bundle stops   
    }

    @Override
    public DMXAdapterTrackerHandler getHandler() {
        return new AppFilesTrackerHandler();
    }

}
