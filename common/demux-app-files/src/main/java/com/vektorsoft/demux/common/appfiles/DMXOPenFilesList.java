/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles;

import java.util.ArrayList;
import java.util.List;

/**
 * Models documents currently opened inside an application.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXOPenFilesList {

    /**
     * List of currently open files.
     */
    private final List<DMXFile> files;

    /**
     * File currently being worked on (selected).
     */
    private DMXFile currentFile;

    /**
     * Creates new instance.
     */
    public DMXOPenFilesList() {
        files = new ArrayList<DMXFile>();
    }

    /**
     * Returns the file that is currently active (in focus).
     * 
     * @return current file
     */
    public DMXFile getCurrentFile() {
        return currentFile;
    }

    /**
     * Sets file to be currently active.
     * 
     * @param currentFile file to set
     */
    public void setCurrentFile(DMXFile currentFile) {
        this.currentFile = currentFile;
    }

    /**
     * Checks if any of the open files is modified.
     * 
     * @return <code>true</code> if there are modified files, <code>false</code> otherwise
     */
    public boolean hasDirtyFiles() {
        for (DMXFile file : files) {
            if (file.isDirty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds new file to the list of open files.
     * 
     * @param file file to add
     */
    public void addFile(DMXFile file) {
        if (files.contains(file)) {
            int index = files.indexOf(file);
            currentFile = files.get(index);
        } else {
            files.add(file);
            currentFile = file;
        }

    }

    /**
     * <p>
     * Closes specified file. When file is closed, file in next or previous index is
     * set as current. If file does not exist in the list, no action is performed.
     * </p>
     *
     * @param file file to close
     */
    public void closeFile(DMXFile file) {
        int index = files.indexOf(file);
        if (index >= 0) {
            files.get(index).close();
            files.remove(index);
            if (files.isEmpty()) {
                currentFile = null;
            } else {
                if (index == files.size()) {
                    currentFile = files.get(index - 1);
                } else {
                    currentFile = files.get(index);
                }
            }

        }
    }
    
    /**
     * Returns the list of all current files.
     * 
     * @return list of files
     */
    public List<DMXFile> getFiles(){
        return files;
    }

}
