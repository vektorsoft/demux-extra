/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.handler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Content handler for files which contain Java serialized objects.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SerializableContentHandler implements DMXFileContentHandler {

    @Override
    public byte[] toByteArray(Object object) {
        if(!(object instanceof Serializable)){
            throw new IllegalArgumentException("Object must be of type java.io.Serializable");
        }
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            ObjectOutputStream oout = new ObjectOutputStream(bout);
            oout.writeObject(object);
            oout.close();
            return bout.toByteArray();
        } catch (IOException iex) {
            throw new RuntimeException(iex);
        }

    }

    @Override
    public Serializable fromByteArray(byte[] data) {
        try {
            ByteArrayInputStream bin = new ByteArrayInputStream(data);
            ObjectInputStream oin = new ObjectInputStream(bin);
            Serializable out = (Serializable) oin.readObject();
            oin.close();
            return out;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public boolean isMimeTypeSupported(String mimeType) {
        if("application/x-java-serialized-object".equals(mimeType) || "application/octet-stream".equals(mimeType)){
            return true;
        }
        return false;
    }
    
    

}
