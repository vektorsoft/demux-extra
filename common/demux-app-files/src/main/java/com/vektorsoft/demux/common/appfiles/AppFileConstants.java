/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles;

/**
 * Contains various constants related to application files management.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public final class AppFileConstants {
    
    /**
     * ID of controller which creates new empty file.
     */
    public static final String CTRL_NEW_FILE = "com.vektorsoft.demux.common.appfiles.NewFileController";
    
    /**
     * ID of controller which saves current file.
     */
    public static final String CTRL_SAVE_FILE = "com.vektorsoft.demux.common.appfiles.FileSaveController";
    
     /**
     * ID of controller for Save as functionality.
     */
    public static final String CTRL_SAVE_AS_FILE = "com.vektorsoft.demux.common.appfiles.FileSaveAsController";
    
    /**
     * ID of controller which closes current file.
     */
    public static final String CTRL_CLOSE_CURRENT_FILE = "com.vektorsoft.demux.common.appfiles.CloseCurrentFileController";
    
    /**
     * ID of controller for opening file.
     */
    public static final String CTRL_OPEN_FILE = "com.vektorsoft.demux.common.appfiles.FileOpenController;";
    
    /**
     * ID for controller for syncing file content.
     */
    public static final String CTRL_SYNC_CONTROLLER = "com.vektorsoft.demux.common.appfiles.SyncController";
    
    /**
     * ID of controller for setting current file.
     */
    public static final String CTRL_SET_CURRENT_FILE = "com.vektorsoft.demux.common.appfiles.SetCurrentController";
    
    /**
     * Name of the variable which holds the list of open files.
     */
    public static final String VAR_OPEN_FILES_LIST = "com.vektorsoft.demux.common.appfiles.open_files_list";
    
    /**
     * ID of model variable which holds current file.
     */
    public static final String VAR_CURRENT_FILE = "com.vektorsoft.demux.common.appfiles.current_file";

    /**
     * Private constructor to prevent instantiation.
     */
    private AppFileConstants(){
        
    }
}
