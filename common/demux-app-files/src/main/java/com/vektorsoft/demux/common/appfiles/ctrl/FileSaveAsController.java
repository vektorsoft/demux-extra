/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.ctrl;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.common.appfiles.DMXOPenFilesList;
import com.vektorsoft.demux.core.dlg.DialogCustomizer;
import com.vektorsoft.demux.core.mva.DMXAbstractController;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.ExecutionException;
import java.io.File;

/**
 * Controller for saving into different file (Save as functionality).
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class FileSaveAsController extends DMXAbstractController {
    
    private final DMXAdapter adapter;
    
    public FileSaveAsController(DMXAdapter adapter){
        super(AppFileConstants.VAR_OPEN_FILES_LIST);
        this.adapter = adapter;
    }

    @Override
    public String getMapping() {
        return AppFileConstants.CTRL_SAVE_AS_FILE;
    }
    

    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        DMXOPenFilesList list = model.get(AppFileConstants.VAR_OPEN_FILES_LIST, DMXOPenFilesList.class);
        DMXFile file = list.getCurrentFile();
        File[] target = adapter.getDialogFactory().createFileChooserDialog(DialogCustomizer.FileChooseType.SAVE, null);
        if (target != null && target.length == 1) {
            file.saveAs(target[0].getAbsolutePath());
        }
         model.set(AppFileConstants.VAR_OPEN_FILES_LIST, list);
    }

}
