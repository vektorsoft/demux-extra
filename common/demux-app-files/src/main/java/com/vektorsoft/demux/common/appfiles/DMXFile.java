/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
 
package com.vektorsoft.demux.common.appfiles;

import com.vektorsoft.demux.common.appfiles.handler.DMXFileContentHandler;
import com.vektorsoft.demux.common.appfiles.handler.internal.FileContentHandlerRegistry;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import eu.medsea.mimeutil.MimeType;
import eu.medsea.mimeutil.MimeUtil2;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Represents opened file in an application. This class models common information about files
 * opened in an application.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public  class DMXFile {
    
    /**
     * Logger for this class.
     */
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(DMXFile.class);
    
    /**
     * Default name of new file.
     */
    private static final String DEFAULT_NAME = "Untitled ";
    
    /**
     * Default size of file save.read buffer.
     */
    private static final int DEFAULT_BUFFER_SIZE = 1024 * 8;
    
    /**
     * Number of currently opened untitled files.
     */
    private static int untitledCount = 1;
    
    /**
     * Mime type detection utility.
     */
    private static final MimeUtil2 MIME;
    
    /**
     * Current file ID.
     */
    private static int currentId = 1;
    
    /**
     * Static initializer.
     */
    static {
        MIME = new MimeUtil2();
        // register mime-util detectors
        MIME.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector"); 
        MIME.registerMimeDetector("eu.medsea.mimeutil.detector.ExtensionMimeDetector"); 
        MIME.registerMimeDetector("eu.medsea.mimeutil.detector.OpendesktopMimeDetector");
    }
    
    
    
    /**
     * Actual file associated with this instance.
     */
    private RandomAccessFile target;
    
    /**
     * Whether file is dirty (modified) or not.
     */
    private boolean dirty;
    
    /**
     * Target file name.
     */
    private String name;
    
    /**
     * File's MIME type.
     */
    private Collection<String> mimeTypes;
    
    /**
     * Target file absolute path.
     */
    private String path;
    
    /**
     * Byte buffer for file system read/write operations.
     */
    private  ByteBuffer buffer;
    
    /**
     * Channel for reading/writing file.
     */
    private FileChannel channel;
    
    /**
     * Current contents of the file.
     */
     private Object currentContent;
     
     /**
      * File content handler.
      */
     private   DMXFileContentHandler contentHandler;
     
    
    /**
     * Creates new empty file with specified MIME type. Default file's name is <code>Untitled xx</code>, where <code>xx</code> is current number 
     * of all Untitled files.
     * 
     * @param mimeType  MIME type
     */
    public  DMXFile(String mimeType){
        target = null;
        dirty = true;
        this.mimeTypes = new ArrayList<String>();
        mimeTypes.add(mimeType);
        untitledIncrement();
        name = DEFAULT_NAME + untitledCount;
        this.contentHandler = FileContentHandlerRegistry.getInstance().getContentHandler(mimeType);
    }
    
    /**
     * Instantiates new instances associated with specified file in file system.
     * 
     * @param file  associated file
     */
    public  DMXFile(File file){
        try {
            target = new RandomAccessFile(file, "rw");
        } catch(FileNotFoundException ex){
            throw new RuntimeException(ex);
        }
        channel = target.getChannel();
        path = file.getAbsolutePath();
        dirty = false;
        name = file.getName();
        buffer = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE);
        Collection<MimeType> raw = MIME.getMimeTypes(file);
        mimeTypes = new ArrayList<String>();
        for(MimeType type : raw){
            mimeTypes.add(type.toString());
        }
        this.contentHandler = FileContentHandlerRegistry.getInstance().getContentHandler(mimeTypes);
    }
    
     /**
     * Increments current count for untitled files.
     */
    private static void untitledIncrement(){
        untitledCount++;
    }

    /**
     * Calculates new file ID.
     * 
     * @return file ID
     */
    private synchronized static int getNextFileId(){
        currentId++;
        return currentId;
    }
    /**
     * Checks if file is modified (dirty).
     * 
     * @return <code>true</code> if file is modified, <code>false</code> otherwise
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     * Returns name of the file.
     * 
     * @return file name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns file's MIME types.
     * 
     * @return MIME type
     */
    public Collection<String> getMimeTypes() {
        return mimeTypes;
    }
    
    /**
     * Returns absolute path of the file.
     * 
     * @return file's absolute path
     */
    public String getPath(){
       return path;
    }
    

    /**
     * Sets underlying file in file systems for this object.
     * 
     * @param target file in file system
     */
    public void setTarget(File target) {
        try {
            this.target = new RandomAccessFile(target, "rw");
            name = target.getName();
            path = target.getAbsolutePath();
            channel = this.target.getChannel();
        } catch(FileNotFoundException ex){
            throw new RuntimeException(ex);
        }
    }

    /**
     * Saves file at new location.
     * 
     * @param newLocation file's new location
     */
    public void saveAs(String newLocation){
        try {
            if(target != null){
                // close file and channel
                target.close();
            }
            
            File f = new File(newLocation);
            target = new RandomAccessFile(f, "rw");
            channel = target.getChannel();
            name = f.getName();
            save();
            // set correct MIME type
            Collection<MimeType> raw = MIME.getMimeTypes(f);
            mimeTypes = new ArrayList<String>();
            for(MimeType type : raw){
                mimeTypes.add(type.toString());
            }
            this.contentHandler = FileContentHandlerRegistry.getInstance().getContentHandler(mimeTypes);
        } catch(FileNotFoundException ex){
            throw new RuntimeException(ex);
        } catch(IOException iex){
            throw new RuntimeException(iex);
        }
        
    }
    
    /**
     * Saves current file content. Content is saved to file determined by {@code target} property.
     * 
     */
    public void save() {
        if(currentContent == null){
            LOGGER.info("No content, nothing to save");
            return;
        }
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            bout.write(contentHandler.toByteArray(currentContent));
            byte[] data = bout.toByteArray();
            bout.close();
           
            // do save to file
            buffer = ByteBuffer.allocate(data.length);
            buffer.clear();
            buffer.put(data);
            buffer.flip();
            
            target.seek(0);
            target.setLength(data.length);
            while (buffer.hasRemaining()) {
                channel.write(buffer);
            }
            channel.force(false);
            dirty = false;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

    }
    
    /**
     * Closes associated file and releases all system resources.
     */
    public void close(){
        try {
            currentContent = null;
            buffer = null;
            if(target != null){
                target.close();
            }
        } catch(IOException ex){
            throw new RuntimeException(ex);
        }
      
      
    }

    /**
     * Sets current content of this file.
     * 
     * @param currentContent content
     */
    public void setCurrentContent(Object currentContent) {
        this.currentContent = currentContent;
        dirty = true;
    }

    /**
     * Returns current content of the file.
     * 
     * @return file content
     */
    public Object getCurrentContent() {
        if(currentContent == null && target != null){
            currentContent = contentHandler.fromByteArray(loadContentAsBytes());
        }
        return currentContent;
    }
    
    /**
     * Loads file content from disk.
     * 
     * @return byte array representing file system
     */
    private byte[] loadContentAsBytes() {
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            
            if (target.length() > DEFAULT_BUFFER_SIZE) {
                buffer = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE);
                int count = 0;
                while ((count = channel.read(buffer)) > 0) {
                    buffer.flip();
                    bout.write(buffer.array(), 0, count);
                    buffer.clear();
                }
            } else {
                buffer = ByteBuffer.allocate((int) target.length());
                channel.read(buffer);
                buffer.rewind();
                buffer.flip();
                bout.write(buffer.array());
            }
            buffer.clear();
            byte[] data = bout.toByteArray();
            bout.close();
            
            return data;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
}
