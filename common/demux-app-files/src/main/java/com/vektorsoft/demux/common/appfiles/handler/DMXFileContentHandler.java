/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.handler;

import com.vektorsoft.demux.common.appfiles.DMXFile;

/**
 * <p>
 *  Defines method for classes which implement conversion of file data into content 
 * which can be manipulated by application. Each {@link DMXFile} contains reference to this 
 * handler in order to perform conversion of bytes to content object.
 * </p>
 * 
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public interface DMXFileContentHandler {
    
    /**
     * Whether specified MIME type is supported by this content handler.
     * 
     * @param mimeType MIME type to check
     * @return <code>true</code> if type is supported, <code>false</code> otherwise
     */
    boolean isMimeTypeSupported(String mimeType);
    
    /**
     * Converts specified object into byte array.
     * 
     * @param object object to convert
     * @return byte array to be written to file
     */
     byte[] toByteArray(Object object);
    
    /**
     * Creates object of type {@code T} from given byte array.
     * 
     * @param data data to convert
     * @return object
     */
    Object fromByteArray(byte[] data);
}
