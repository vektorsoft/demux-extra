/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.bundle;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.common.appfiles.DMXOPenFilesList;
import com.vektorsoft.demux.common.appfiles.ctrl.CloseCurrentFileController;
import com.vektorsoft.demux.common.appfiles.ctrl.FileOpenController;
import com.vektorsoft.demux.common.appfiles.ctrl.FileSaveAsController;
import com.vektorsoft.demux.common.appfiles.ctrl.FileSaveController;
import com.vektorsoft.demux.common.appfiles.ctrl.NewFileController;
import com.vektorsoft.demux.common.appfiles.ctrl.SetSelectedFileController;
import com.vektorsoft.demux.common.appfiles.ctrl.SyncController;
import com.vektorsoft.demux.core.app.DMXAdapterTrackerHandler;
import com.vektorsoft.demux.core.mva.DMXAdapter;

/**
 * Simple implementation of {@code ServiceTracker} used to track changes to {@code DMXAdapter} service.
 * 
 */
public class AppFilesTrackerHandler extends DMXAdapterTrackerHandler {

    @Override
    public void onAdapterAdded(DMXAdapter adapter) {
       adapter.registerData(AppFileConstants.VAR_OPEN_FILES_LIST, new DMXOPenFilesList());
       adapter.registerData(AppFileConstants.VAR_CURRENT_FILE, null);
       adapter.registerController(new NewFileController());
       adapter.registerController(new FileSaveController(adapter));
       adapter.registerController(new FileSaveAsController(adapter));
       adapter.registerController(new CloseCurrentFileController());
       adapter.registerController(new FileOpenController(adapter));
       adapter.registerController(new SyncController());
       adapter.registerController(new SetSelectedFileController());
    }

    @Override
    public void onAdapterModified(DMXAdapter adapter) {
        // add here logic needed to run when adapter is modfied
    }

    @Override
    public void onAdapterRemoved(DMXAdapter adapter) {
        // add here logic needed to run when adapter is removed
    }

}
