/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.ctrl;

import com.vektorsoft.demux.common.appfiles.AppFileConstants;
import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.common.appfiles.DMXOPenFilesList;
import com.vektorsoft.demux.core.mva.DMXAbstractController;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.ExecutionException;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SetSelectedFileController extends DMXAbstractController {
    
    public SetSelectedFileController(){
        super(AppFileConstants.VAR_OPEN_FILES_LIST);
    }

    @Override
    public String getMapping() {
        return AppFileConstants.CTRL_SET_CURRENT_FILE;
    }
    
    

    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        DMXOPenFilesList list = model.get(AppFileConstants.VAR_OPEN_FILES_LIST, DMXOPenFilesList.class);
        list.setCurrentFile((DMXFile)params[0]);
        model.set(AppFileConstants.VAR_CURRENT_FILE, list.getCurrentFile());
    }

}
