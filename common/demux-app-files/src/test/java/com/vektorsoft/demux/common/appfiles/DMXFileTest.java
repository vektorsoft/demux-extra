/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.common.appfiles;

import com.vektorsoft.demux.common.appfiles.handler.TextContentHandler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 * Provides test case for {@link DMXFile} class.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXFileTest {
    
    private static File bigTextFile;
    private static TextContentHandler textHandler;
    
    @BeforeClass
    public static void setUpClass() throws Exception{
        bigTextFile = new File("bigfile.txt");
        PrintWriter writer = new PrintWriter(bigTextFile);
        for(int i = 0;i < 5000;i++){
            writer.println("uuid=" + UUID.randomUUID().toString());
        }
        writer.close();
        textHandler = new TextContentHandler();
    }
    
    @AfterClass
    public static void cleanUpClass(){
        bigTextFile.delete();
    }
    
    /**
     * <p>
     * Verified correct naming of new files. Each new file is named as <code>Untitled xx</code>, where <code>xx</code> is 
     * current number of untitled files. This number is incremented with each new file. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Create 2 new files</li>
     *  <li>Verify that both start with "Untitled" </li>
     *  <li>Verify that file 2 index is greater then file 1 index</li>
     * </ol>
     */
    @Test
    public void testUntitledFile(){
        DMXFile file1 = new DMXFile("text/plain");
        DMXFile file2 = new DMXFile("text/plain");
        assertTrue("Invalid file name", file1.getName().startsWith("Untitled"));
        assertTrue("Invalid file name", file2.getName().startsWith("Untitled"));
        int index1 = Integer.parseInt(file1.getName().split(" ")[1]);
        int index2 = Integer.parseInt(file2.getName().split(" ")[1]);
        
        assertTrue("Invalid index", index2 > index1);
    }
    
    /**
     * <p>
     * Verifies correct handling of existing files. When existing file is opened, it's path, name and MIME type should
     * be correctly interpreted. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Open existing text file</li>
     *  <li> Verify that name, path and MIME type are correct</li>
     *  <li>read file contents and verify they match existing</li>
     * </ol>
     */
    @Test
    public void testExistingFile() throws Exception{
        File file = new File(this.getClass().getResource("/textfile.txt").toURI());
        DMXFile dmxfile = new DMXFile(file);
        
        assertEquals("Invalid file name", file.getName(), dmxfile.getName());
        assertEquals("Invalid file path", file.getAbsolutePath(), dmxfile.getPath());
        assertTrue("Invlaid MIME type",  dmxfile.getMimeTypes().contains("text/plain"));
        assertFalse("File marked as dirty", dmxfile.isDirty());
    }
    
    /**
     * <p>
     *  Verifies that file contents are correctly read. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Create {@code DMXFile} from test file</li>
     *  <li>Read contents and verify they match expected</li>
     * </ol>
     */
    @Test
    public void getFileContentsTest() throws Exception{
        File f = new File(this.getClass().getResource("/textfile.txt").toURI());
        BufferedReader br = new BufferedReader(new FileReader(f));
        String line = br.readLine();
        br.close();
        
        DMXFile file = new DMXFile(f);
        String content = (String)file.getCurrentContent();
        assertEquals("Content mismatch", line, content);
    }
    
    /**
     * <p>
     *  Verifies correct eading of files larger then default buffer (8 KB). Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Read in the contents of big file</li>
     *  <li>create {@code DMXFile}, read contents and verify they are equal</li>
     * </ol>
     */
    @Test
    public void getBigFileContentTest() throws Exception{
        BufferedReader reader = new BufferedReader(new FileReader(bigTextFile));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while((line = reader.readLine()) != null){
            sb.append(line).append("\n");
        }
        reader.close();
        
        DMXFile dmxfile = new DMXFile(bigTextFile);
        String content = (String)dmxfile.getCurrentContent();
        assertEquals("Content mismatch", sb.toString(), content);
    }
    
    /**
     * <p>
     * Verifies correct setting of current content. This reflects current state of model data. Steps taken in 
     * this test:
     * </p>
     * <ol>
     *  <li>Create new {@code DMXFile} with empty content.</li>
     *  <li>set new content and verify it is correct</li>
     * </ol>
     */
    @Test
    public void contentTest(){
        DMXFile file = new DMXFile("text/plain");
        String content = (String)file.getCurrentContent();
        assertNull("Content is not null", content);
        
        file.setCurrentContent("new content");
        assertEquals("Content mismatch", "new content", file.getCurrentContent());
    }
    
    /**
     * <p>
     *  Verifies that file is correctly save to disk. The following steps are performed in this test:
     * </p>
     * <ol>
     *  <li>Create new {@code DMXFile}</li>
     *  <li>Set new content and save file</li>
     *  <li>Verify that new content is persisted</li>
     * </ol>
     */
    @Test
    public void fileSaveTest() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        File f = new File(this.getClass().getResource("/textfile.txt").toURI());
        DMXFile file = new DMXFile(f);
        String newContent = "Content saved at " + sdf.format(new Date());
        file.setCurrentContent(newContent);
        file.save();
        
        //verify that content matches saved
        BufferedReader reader = new BufferedReader(new FileReader(f));
        String line = reader.readLine();
        reader.close();
        assertEquals("Saved content mismatch", newContent, line);
    }
    
    
    /**
     * <p>
     *  Verifies correct behavior of "Save as" functionality. In this case, current contents are save into
     * different file, leaving current file unchanged. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>read current contents of a file</li>
     *  <li>modify content and save it as different file</li>
     *  <li>Verify that saved contents are correct and that original file is unchanged</li>
     *  <li>delete newly generated file</li>
     * </ol>
     * 
     * @throws Exception if an error occurs
     */
    @Test
    public void saveAsTest() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        File f = new File(this.getClass().getResource("/textfile.txt").toURI());
        
        BufferedReader br = new BufferedReader(new FileReader(f));
        String original = br.readLine();
        br.close();
        
        DMXFile dmxfile = new DMXFile(f);
        String newContent = "Content saved at " + System.nanoTime();
        dmxfile.setCurrentContent(newContent);
        dmxfile.saveAs("newfile.txt");
        
        // verify original content
        br = new BufferedReader(new FileReader(f));
        String line = br.readLine();
        br.close();
        assertEquals("Invalid old file content", original, line);
        // erify new content
        File newFile = new File("newfile.txt");
        assertTrue("New file does not exist", newFile.exists());
        br = new BufferedReader(new FileReader(newFile));
        line = br.readLine();
        assertEquals("Invalid new content", line, newContent);
        newFile.delete();
        
    }
}
