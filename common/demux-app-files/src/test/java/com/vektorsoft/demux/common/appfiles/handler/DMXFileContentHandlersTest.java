/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.handler;

import com.vektorsoft.demux.common.appfiles.DMXFile;
import com.vektorsoft.demux.common.appfiles.SerializableClass;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Provides unit tests for basic implementations of {@link DMXFileContentHandler}.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXFileContentHandlersTest {
    
    private SerializableContentHandler serializableHandler;
    
    @Before
    public void setup(){
        serializableHandler = new SerializableContentHandler();
    }

    @Test
    public void serializableContentHandlerTest(){
        DMXFile dmxfile = new DMXFile("application/x-java-serialized-object");
        assertNull("Content is not null", dmxfile.getCurrentContent());
        
        SerializableClass clazz = new SerializableClass();
        clazz.setNum(23);
        clazz.setText("some text");
        dmxfile.setCurrentContent(clazz);
        
        assertNotNull("Content is null", dmxfile.getCurrentContent());
        SerializableClass out = (SerializableClass)dmxfile.getCurrentContent();
        assertEquals("Invalid integer", 23, out.getNum());
        assertEquals("Invalid text", "some text", out.getText());
    }
    
    @Test
    public void serializableContentLoad() throws Exception {
        SerializableClass clazz = new SerializableClass();
        clazz.setNum(56);
        clazz.setText("more text");
        // persist
        File file = new File("output.ser");
        ObjectOutputStream oout = new ObjectOutputStream(new FileOutputStream(file));
        oout.writeObject(clazz);
        oout.close();
        
        DMXFile dmxfile = new DMXFile(file);
        SerializableClass out = (SerializableClass)dmxfile.getCurrentContent();
        assertEquals("Invalid integer", 56, out.getNum());
        assertEquals("invalid text", "more text", out.getText());
        
        // make changes and save again
        out.setNum(78);
        out.setText("another text");
        dmxfile.setCurrentContent(out);
        dmxfile.save();
        dmxfile.close();
        
        // load changed file and verify
        ObjectInputStream oin = new ObjectInputStream(new FileInputStream(file));
        SerializableClass out1 = (SerializableClass)oin.readObject();
        oin.close();
        assertEquals("Invalid integer", 78, out1.getNum());
        assertEquals("invalid text", "another text", out1.getText());
        file.delete();
    }
}
