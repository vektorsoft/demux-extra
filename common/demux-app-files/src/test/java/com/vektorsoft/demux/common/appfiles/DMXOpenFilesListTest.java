/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles;

import com.vektorsoft.demux.common.appfiles.handler.DMXFileContentHandler;
import com.vektorsoft.demux.common.appfiles.handler.TextContentHandler;
import java.lang.reflect.Field;
import java.util.List;
import org.junit.Before;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Provides test cases for {@link DMXOPenFilesList} class.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXOpenFilesListTest {
    
    private DMXOPenFilesList list;
    private DMXFileContentHandler contentHandler;
    
    @Before
    public void setup(){
        list = new DMXOPenFilesList();
        contentHandler = new TextContentHandler();
    }

    /**
     * <p>
     *  Verifies correct addition of new file to list of open files. When new file is added, it 
     * should be set as current file. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Add new file to list and verify it is current</li>
     *  <li>Repeat procedure and verify for each new file that it is current</li>
     *  <li>verify that last change event is correct</li>
     * </ol>
     */
    @Test
    public void addFileTest(){
        DMXFile file1 = new DMXFile("text/plain");
        list.addFile(file1);
        DMXFile current = list.getCurrentFile();
        assertEquals("Invalid current file", current, file1);
        
        DMXFile file2 = new DMXFile("text/plain");
        list.addFile(file2);
        current = list.getCurrentFile();
        assertEquals("Invalid current file", current, file2);
    }
    
    /**
     * <p>
     *  Verifies correct adding of the file which is already present in the list. In this case, no new file 
     * should be added and existing file should be set as current. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>add 2 new files to list. Verify that second file is current.</li>
     *  <li>add first file again. Verify that file is not added again, and that it is set as current.</li>
     * </ol>
     */
    @Test
    public void addExistingFileTest() throws Exception{
        DMXFile file1 = new DMXFile("text/plain");
        DMXFile file2 = new DMXFile("text/plain");
        
        list.addFile(file1);
        list.addFile(file2);
        // verify that second file is current
        assertEquals("Invalid current file", file2, list.getCurrentFile());
        
        // add first file again
        list.addFile(file1);
        // verify that no new files are added
        Field listField = list.getClass().getDeclaredField("files");
        listField.setAccessible(true);
        List fileList = (List)listField.get(list);
        assertEquals("Invalid list size", 2, fileList.size());
        // verify current file
        assertEquals("Invalid current file after ass", file1, list.getCurrentFile());
    }
    
    /**
     * <p>
     *  Verifies correct closing of file. When file is closed, it should be removed from the list. File at the next 
     * index is set as current. If next index exists, file at previous index is set as current. If no other file,
     * exists, list is cleared, and no current file is set. 
     * </p>
     * <p>
     *  Steps taken in this test:
     * </p>
     * <ol>
     *  <li>add 3 files to list</li>
     *  <li>Close second file. Verify that next one is set as current</li>
     *  <li>Close last file. Verify that previous is set as current</li>
     *  <li>Close last file. Verify that current file is <code>null</code></li.
     * </ol>
     */
    @Test
    public void closeFileTest(){
        DMXFile file1 = new DMXFile("text/plain");
        DMXFile file2 = new DMXFile("text/plain");
        DMXFile file3 = new DMXFile("text/plain");
        
        list.addFile(file1);
        list.addFile(file2);
        list.addFile(file3);
        assertEquals("Invalid current file at start", file3, list.getCurrentFile());
        
        // close second file and verify that file3 is current
        list.setCurrentFile(file2);
        list.closeFile(file2);
        assertEquals("Invalid current file after file2 closed", file3, list.getCurrentFile());
        
        // close file3 and verify that file1 is current
        list.closeFile(file3);
        assertEquals("Invalid current file after file3 closed", file1, list.getCurrentFile());
        
        list.closeFile(file1);
        assertNull("current fiel not null", list.getCurrentFile());
    }
    
}
