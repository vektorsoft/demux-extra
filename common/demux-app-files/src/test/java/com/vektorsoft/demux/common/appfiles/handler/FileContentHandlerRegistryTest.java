/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektorsoft Ltd..
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.common.appfiles.handler;

import com.vektorsoft.demux.common.appfiles.handler.internal.FileContentHandlerRegistry;
import org.junit.Before;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Contains test cases for {@link FileContentHandlerRegistry} class.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class FileContentHandlerRegistryTest {

   
    
    /**
     * Verifies that correct content handler is returned for text content type. It should be an
     * instance of {@link TextContentHandler}.
     */
    @Test
    public void verifyTextType(){
        DMXFileContentHandler handler = FileContentHandlerRegistry.getInstance().getContentHandler("text/plain");
        assertTrue("Invalid content handler", (handler instanceof TextContentHandler));
    }
    
    /**
     * Verifies that content handler is correctly registered.
     */
    @Test
    public void testHandlerRegistration(){
        DummyContentHandler dummy = new DummyContentHandler();
        FileContentHandlerRegistry.getInstance().registerContentHandler(dummy);
        
        // verify that correct is fetched
        DMXFileContentHandler out = FileContentHandlerRegistry.getInstance().getContentHandler("application/sometype");
        assertTrue("Inalid content handler", (out instanceof DummyContentHandler));
    }
    
    /**
     * Verifies correct handling of unknown MIME type. An exception should be thrown in this case.
     */
    @Test (expected = IllegalArgumentException.class)
    public void testUnknownType(){
        DMXFileContentHandler out = FileContentHandlerRegistry.getInstance().getContentHandler("unknown/type");
        fail("Did not throw exception");
    }
    
    /**
     * Dummy class used for testing.
     */
    private static class DummyContentHandler implements DMXFileContentHandler {

        @Override
        public boolean isMimeTypeSupported(String mimeType) {
            return mimeType.equals("application/sometype");
        }

        @Override
        public byte[] toByteArray(Object object) {
            return new byte[0];
        }

        @Override
        public Object fromByteArray(byte[] data) {
            return null;
        }
        
    }
}
