
# DEMUX Framework Extra components

This project contains reusable components for DEMUX Framework applications. These components provide functions commonly found in
desktop, web and mobile applications. To use these components within DEMUX Framework applications, it is normally sufficient to 
simply include component as Maven dependency, and it will be configured and deployed automatically.
